# Vođenje evidencije o studentima i njihovim aktivnostima

### Web stranica
Projekat je urađen korištenjem HTML5, CSS3, JavaScript uz Node.js okruženje i MySQL bazu podataka.
Napravili smo 8 različitih HTML stranica(login, pregled studenata, odabir vježbe, pregled commit-a, dodavanje zadataka, dodavanje vježbe/spirale, dodavanje studenata, dodavanje godine)
uz odgovarajuće CSS-ove.
Korištenjem JavaScripta napravljena je validacija formi, te dinamičke izmjene na stranici pregleda commit-a.
Node.js je omogućio asinhroni rad web stranice, bez čekanja i nerviranja da se izvrše neke zahtjevane aktivnosti.
Radili smo razna rutiranja na serveru korištenjem express paketa unutar Node-a, te pravili GET i POST zahtjeve, handle tih zahtjeva, 
kao i komunikaciju sa MySQL bazom podataka.

Web aplikacija je urađena spiralnim načinom rada, sastoji se od spirala1, spirala2, spirala3 i na kraju finalna verzija spirala4.
### Branches
 - spirala1 - HTML5 i CSS3
 - spirala2 - JavaScript
 - spirala3 - Node.js
 - spirala4 - MySQL